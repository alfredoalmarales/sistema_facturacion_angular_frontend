import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClientesComponent } from './pages/lista-clientes/clientes.component';
import { CrearclienteComponent } from './pages/crear-cliente/crearcliente.component';
import { HomeComponent } from './pages/home/home.component';
import { AuthGuard } from './guards/auth.guard';
import { PerfilGuard } from './guards/perfil.guard';
import { DetalleFacturaComponent } from './pages/detalle-factura/detalle-factura.component';
import { FacturasComponent } from './pages/crear-facturas/facturas.component';
import { ListaProductosComponent } from './pages/lista-productos/lista-productos.component';
import { CrearProductosComponent } from './pages/crear-productos/crear-productos.component';
import { FacturacionComponent } from './pages/facturacion/facturacion.component';
import { UsuariosComponent } from './pages/lista-usuarios/usuarios.component';
import { CrearUsuarioComponent } from './pages/crear-usuario/crear-usuario.component';

const routes: Routes = [
  {path: '',component:HomeComponent},

  {path: 'usuarios', component: UsuariosComponent,canActivate:[AuthGuard,PerfilGuard],data:{perfil:'ROLE_ADMIN'}},
  {path: 'usuarios/page/:page/:term', component: UsuariosComponent,canActivate:[AuthGuard,PerfilGuard],data:{perfil:'ROLE_ADMIN'}},
  {path: 'crearUsuarios', component:CrearUsuarioComponent,canActivate:[AuthGuard,PerfilGuard],data:{perfil:'ROLE_ADMIN'}},
  {path: 'editUsuarios/:id', component:CrearUsuarioComponent,canActivate:[AuthGuard,PerfilGuard],data:{perfil:'ROLE_ADMIN'}},

  {path: 'clientes', component: ClientesComponent,canActivate:[AuthGuard]},
  {path: 'clientes/page/:page/:term', component: ClientesComponent,canActivate:[AuthGuard]},
  {path: 'crearClientes', component:CrearclienteComponent,canActivate:[AuthGuard,PerfilGuard],data:{perfil:'ROLE_ADMIN'}},
  {path: 'editClientes/:id', component:CrearclienteComponent,canActivate:[AuthGuard,PerfilGuard],data:{perfil:'ROLE_ADMIN'}},
  
  {path: 'productos', component: ListaProductosComponent,canActivate:[AuthGuard]},
  {path: 'productos/page/:page/:term', component: ListaProductosComponent,canActivate:[AuthGuard]},
  {path: 'crearProductos', component:CrearProductosComponent,canActivate:[AuthGuard,PerfilGuard],data:{perfil:'ROLE_ADMIN'}},
  {path: 'editProductos/:id', component:CrearProductosComponent,canActivate:[AuthGuard,PerfilGuard],data:{perfil:'ROLE_ADMIN'}},
  
  {path: 'facturas/:id', component: DetalleFacturaComponent,canActivate:[AuthGuard]},
  {path: 'facturas/form/:clienteId', component: FacturasComponent,canActivate:[AuthGuard]},

  {path: 'facturacion', component: FacturacionComponent,canActivate:[AuthGuard]},

  {path:'**',  redirectTo:''},
 


  // {path:'login',component:LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
