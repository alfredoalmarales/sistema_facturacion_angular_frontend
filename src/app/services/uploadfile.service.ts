import { HttpClient, HttpEvent, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UploadfileService {
  private urlEndPoint: string = 'http://localhost:8080/sitiapp';
  
  constructor(private http: HttpClient) { }

    /**
   * Esta función sube la foto de un producto 
   * @param {File} archivo la foto eligida 
   * @param {number} id  id del producto que le asignaremos la foto
   * @returns {Observable<HttpEvent<{}>>} retorna un observable HttpEvent
   */
     subirFoto(archivo: File, id: number,entidad:string): Observable<HttpEvent<{}>> {
      let formData = new FormData();
      formData.append("archivo", archivo);
      formData.append("id", id.toString());
  
      const req = new HttpRequest('POST', `${this.urlEndPoint}/${entidad}/uploads`, formData, {
        reportProgress: true
      });
  
      return this.http.request(req);
    }
}
