import { HttpClient, HttpEvent, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Producto } from '../models/producto';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  private urlEndPoint: string = 'http://localhost:8080/sitiapp/productos';

  constructor(
    private http: HttpClient,
    private router: Router,
  ) { }

  /**
   * Esta función obtiene una paginación de todos los productos que coincidan con el nombre buscado
   * @param {string} nombrebuscado nombre del producto a buscar
   * @param {number} page número de la pagina en la busqueda
   * @returns {Observable<any>} retorna un obserbable de una paginación
   */
  getProductosByName(nombrebuscado: string, page: number): Observable<any> {
    return this.http.get(this.urlEndPoint + '/page/' + page + '/' + nombrebuscado).pipe(
      map((response: any) => {
        response.content as Producto[];
        return response;
      }
     )
    )
  }

  /**
   * Esta función crea el producto en la base de datos
   * @param {Producto} producto producto a registrar
   * @returns {Observable<Producto>} retorna un observable del producto creado
   */

  create(producto: Producto): Observable<Producto> {
    return this.http.post(this.urlEndPoint, producto).pipe(
      map((response: any) => response.producto as Producto),
      catchError(e => {
        if (e.status == 400) {
          return throwError(e);
        }
        if (e.error.mensaje) {
          console.error(e.error.mensaje)
        }
        return throwError(e);
      })
    );
  }

  /**
   * Esta función obtiene un producto de la base de datos
   * @param id id del producto a obtener 
   * @returns {Observable<Producto>} retorna un observable con el producto obtenido
   */

  getProducto(id: any): Observable<Producto> {
    return this.http.get<Producto>(`${this.urlEndPoint}/${id}`).pipe(
      catchError(e => {
        if(e.status!=401 && e.error.mensaje){
          const textoerror=e.error.mensaje;
          console.error(textoerror)
        }     
        return throwError(e);
      })
    )
  }

  /**
   * Esta función actualiza un producto en la base de datos
   * @param {Producto} producto producto que se va actualizar
   * @returns {Observable<any>} retorna un observable de tipo any
   */
  update(producto: Producto): Observable<any> {
    return this.http.put<any>(`${this.urlEndPoint}/${producto.id}`, producto).pipe(
      catchError(e => {

        if (e.error.mensaje) {
          console.error(e.error.mensaje)
        }
        if (e.status == 400) {
          return throwError(e);
        }

        return throwError(e);
      })
    )
  }


}
