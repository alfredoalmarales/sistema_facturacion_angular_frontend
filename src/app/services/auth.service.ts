import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { UsuarioLogin } from '../models/usuario-login';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  /**
   * Esta variable almacena los datos del usuario logueado
   */
  private _usuariologin!: UsuarioLogin;
  /**
  * Esta variable almacena los datos del token del usuario logueado
  */
  private _token: string = "";

  constructor(private http: HttpClient) { }

  /**
   * Esta función sirve para obtener el usuario logueado
   * @returns {UsuarioLogin} retorna el usuario logueado o un usuario vacio si no hay
   */
  public get usuario(): UsuarioLogin {
    if (this._usuariologin != null) {
      return this._usuariologin;
    } else if (this._usuariologin == null && sessionStorage.getItem('usuario') != null) {
      this._usuariologin = JSON.parse(sessionStorage.getItem('usuario')!) as UsuarioLogin
      return this._usuariologin;
    }
    return new UsuarioLogin()
  }

  /**
  * Esta función sirve para obtener el token del usuario logueado
  * @returns {string} retorna token del usuario logueado o una cadena vacia si no hay
  */
  public get token(): string {
    if (this._token != "") {
      return this._token;
    } else if (this._token == "" && sessionStorage.getItem('token') != null) {
      this._token = sessionStorage.getItem('token')!
      return this._token;
    }
    return "";
  }

  /**
   * Esta función nos sirve para loguearnos en nuestra aplicación
   * @param {string} username username del usuario a loguearse
   * @param {string} password observable del usuario a loguearse
   * @returns {Observable<any>} retorna un observable any
   */
  login(username: string, password: string): Observable<any> {
    const urlEndPoint = 'http://localhost:8080/oauth/token';

    const credenciales = btoa('angularapp' + ':' + '12345');

    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      'Authorization': 'Basic ' + credenciales
    })

    let params = new URLSearchParams();
    params.set('grant_type', 'password')
    params.set('username', username)
    params.set('password', password)

    return this.http.post<any>(urlEndPoint, params.toString(), { headers: httpHeaders })
  }

  /**
   * Esta función nos sirve para guardar los datos del usuario logueado en el sessionStorage
   * @param {string} accesToken token de acceso 
   */
  guardarUsuario(accesToken: string): void {
    let payload = this.obtenerDatosToken(accesToken);
    this._usuariologin = {
      nombre: payload.nombre,
      apellido: payload.apellido,
      username: payload.user_name,
      perfil: payload.authorities,
    }
    sessionStorage.setItem('usuario', JSON.stringify(this.usuario))
  }

  /**
   * Esta función nos sirve para guardar el token en el sessionStorage
   * @param {string} accesToken token de acceso  
   */
  guardarToken(accesToken: string): void {
    this._token = accesToken;
    sessionStorage.setItem('token', accesToken)
  }

  /**
   * Esta función nos sirve para obtener los datos de nuestro token de acceso
   * @param {string} accesToken token de acceso
   * @returns {any} retorna any
   */
  obtenerDatosToken(accesToken: string): any {
    if (accesToken != null) {
      return JSON.parse(atob(accesToken.split(".")[1]))
    }
    return null;
  }

  /**
   * Esta función nos sirve para verificar si un usuario esta autentificado
   * @returns {boolean} retorna true si lo esta o false si no lo esta
   */
  isAuthenticated(): boolean {
    if (this.token == "") {
      return false
    }
    let payload = this.obtenerDatosToken(this.token);
    if (payload != null && payload.user_name && payload.user_name.length > 0) {
      return true;
    }
    return false;
  }

  /**
   * Esta función nos sirve para saber si un usaurio tiene el perfil que se envia como argumento
   * @param {string} perfil perfil a verificar 
   * @returns {boolean} retorna true si tiene ese perfil o de lo contrario false 
   */
  tienePerfil(perfil: string): boolean {
    // if (this.usuario.perfil?.nombre?.includes(perfil)) {
    if (this.usuario.perfil?.includes(perfil)) {
      return true
    }
    return false
  }

  /**
   * Esta función nos sirve para cerrar sesión en nuestra aplicación
   */
  logout(): void {
    this._token = "";
    this._usuariologin = {
      nombre: "",
      apellido: "",
      username: "",
      perfil: "",
    }
    sessionStorage.clear();
  }

}
