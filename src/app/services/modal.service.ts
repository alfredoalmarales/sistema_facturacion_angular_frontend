import { EventEmitter, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ModalService {
  /**
   * Esta variable representa si la ventana modal esta abierta(true) o cerrada(false)
   */
  modal:boolean=false;
  private _notificarUpload= new EventEmitter<any>()
  constructor() { }

  /**
   * Esta función nos sirve para notificar a los componentes que estan suscriptos 
   * que se cargo subio una imagen y deben actualizarse
   */
  get notificarUpload():EventEmitter<any>{
    return this._notificarUpload;
  }

  abrirModal(){
     this.modal=true;
  }
  cerrarModal(){
    this.modal=false;
  }
}
