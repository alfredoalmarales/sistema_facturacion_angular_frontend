import { Injectable } from '@angular/core';
import { HttpClient, HttpEvent, HttpRequest } from '@angular/common/http';
import { map, catchError, tap } from 'rxjs/operators';
import { Cliente } from '../models/cliente';
import { Observable, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { TipoIdentificacion } from '../models/tipo-identificacion';
@Injectable({
  providedIn: 'root'
})

export class ClienteService {

  private urlEndPoint: string = 'http://localhost:8080/sitiapp/clientes';

  constructor(
    private http: HttpClient,
    private router: Router,
  ) { }

  /**
   * Esta función sirve para hacer una petición get y obterer los tipos de identificaciones que hay 
   * @returns 
   */
  getTipoIdentificaciones(): Observable<TipoIdentificacion[]> {
    return this.http.get<TipoIdentificacion[]>(this.urlEndPoint + '/tipo_identificaciones');
  }

  /**
   * Esta función hace una peticion get para obtener una paginacion de los clientes que coincidan con el nombre buscado
   * @param {string} nombrebuscado nombre del cliente a buscar 
   * @param  {page} page número de la pagina 
   * @returns {Observable<any>} retorna un observable any
   */
  getClientesByName(nombrebuscado: string, page: number): Observable<any> {
    return this.http.get(this.urlEndPoint + '/page/' + page + '/' + nombrebuscado).pipe(
      map((response: any) => {
        response.content as Cliente[];
        return response;
      }
      )
    )
  }


  /**
    * Esta función hace una petición post para registrar un cliente en la base de datos
    * @param {Cliente} cliente cliente a registrar
    * @returns {Observable<Cliente>} retorna un observable cliente
    */
  create(cliente: Cliente): Observable<Cliente> {
    return this.http.post(this.urlEndPoint, cliente).pipe(
      map((response: any) => response.cliente as Cliente),
      catchError(e => {
        if (e.status == 400) {
          return throwError(e);
        }
        if (e.error.mensaje) {
          console.error(e.error.mensaje)
        }
        return throwError(e);
      })
    );
  }

  /**
   * Esta función hace una peticón get para obtener un cliente que coincida 
   * con el tipo de identificación e identificación buscados
   * @param {number} tipoiden tipo de identificación del cliente buscado   
   * @param {string} iden identificación del cliente buscado
   * @returns {Observable<Cliente>} retorna un observable cliente
   */
  findCliente(tipoiden: number, iden: string): Observable<Cliente> {
    return this.http.get<Cliente>(`${this.urlEndPoint}/${tipoiden}/${iden}`).pipe(
      catchError(e => {
        if (e.status != 401 && e.error.mensaje) {
          const textoerror=e.error.mensaje;
          console.error(textoerror)
        }
        return throwError(e);
      })
    )
  }

  /**
   * Esta función hace una petición get para obtener un cliente por medio de su id
   * @param {number} id id del cliente a obtener
   * @returns {Observable<Cliente>} retorna un observable cliente
   */
  getCliente(id: number): Observable<Cliente> {
    return this.http.get<Cliente>(`${this.urlEndPoint}/${id}`).pipe(
      catchError(e => {
        if (e.status != 401 && e.error.mensaje) {
          this.router.navigate(['/clientes']);
          const textoerror = e.error.mensaje;
          console.error(textoerror)
        }
        return throwError(e);
      })
    )
  }

  /**
   * Esta función hace una peticón put para actualizar un cliente 
   * @param {Cliente} cliente cliente que se va actualizar
   * @returns {Observable<any>} retorna un observable any
   */
  update(cliente: Cliente): Observable<any> {
    return this.http.put<any>(`${this.urlEndPoint}/${cliente.id}`, cliente).pipe(
      catchError(e => {

        if (e.error.mensaje) {
          console.error(e.error.mensaje)
        }
        if (e.status == 400) {
          return throwError(e);
        }

        return throwError(e);
      })
    )
  }
}

