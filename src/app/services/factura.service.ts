import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Factura } from '../models/factura';
import { Producto } from '../models/producto';

@Injectable({
  providedIn: 'root'
})
export class FacturasService {

  private urlEndPoint: string = 'http://localhost:8080/sitiapp/facturas';
 
  constructor(private http: HttpClient) { }

  /**
   * Esta función obtiene una factura por medio de su id
   * @param id id de la factura a obtener
   * @returns {Observable<Factura>} retorna un observable factura
   */
  getFactura(id:any):Observable<Factura>{
    return this.http.get<Factura>(`${this.urlEndPoint}/${id}`);
  }
  /**
   * Elimina una factura de la base de datos por medio de su id
   * @param id de la factura a eliminar
   * @returns{Observable<void>} retorna observable void
   */
  deleteFactura(id:number):Observable<void>{
   return this.http.delete<void>(`${this.urlEndPoint}/${id}`);
  }

  /**
   * Esta función hace una petición get para traer los productos que coincidan con el nombre buscado
   * @param {string} term nombre del producto a buscar
   * @returns {Observable<Producto[]>} retorna un observable producto
   */
  filtrarProductos(term:string):Observable<Producto[]>{
    return this.http.get<Producto[]>(`${this.urlEndPoint}/filtrar-productos/${term}`);
  }

  /**
   * Esta función hace una petición post para registarr una nueva factura
   * @param {Factura} factura factura a crear
   * @returns 
   */
  createFactura(factura:Factura):Observable<Factura>{
    return this.http.post<Factura>(this.urlEndPoint,factura);
  }
}
