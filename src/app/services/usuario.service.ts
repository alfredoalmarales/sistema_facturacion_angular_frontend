import { Injectable } from '@angular/core';
import { Usuario } from '../models/usuario';
import { HttpClient, HttpEvent, HttpRequest } from '@angular/common/http';
import { map, catchError, tap } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { Perfil } from '../models/perfil';


@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  private urlEndPoint: string = 'http://localhost:8080/sitiapp/usuarios';
  constructor(private http: HttpClient,
    private router: Router,
  ) { }


  /**
   * esta función hace una petición al servidor para traer los tipos de perfiles
   * @returns {Observable<Perfil[]>} retorna un obserbable con la lista de todos los tipos de perfiles      
   */
  getTipoPerfiles(): Observable<Perfil[]> {
    return this.http.get<Perfil[]>(this.urlEndPoint + '/tipos_perfiles');
  }

  /**
   * Esta función obtiene una paginación de todos los usuarios que coincidan con el username buscado
   * @param {string} username username del usuario a buscar
   * @param {number} page número de la pagina en la busqueda
   * @returns {Observable<any>} retorna un obserbable de una paginación
   */
  getUsuariosByName(username: string, page: number): Observable<any> {
    return this.http.get(this.urlEndPoint + '/page/' + page + '/' + username).pipe(
      map((response: any) => {
        response.content as Usuario[];
        return response;
      }
      )
    )
  }

  /**
   * Esta función crea el usuario en la base de datos
   * @param {Usuario} usuario usuario a registrar
   * @returns {Observable<Usuario>} retorna un observable del usuario creado
   */
  create(usuario: Usuario): Observable<Usuario> {
    return this.http.post(this.urlEndPoint, usuario).pipe(
      map((response: any) => response.usuario as Usuario),
      catchError(e => {
        if (e.status == 400) {
          return throwError(e);
        }
        if (e.error.mensaje) {
          console.error(e.error.mensaje)
        }
        return throwError(e);
      })
    );
  }

  /**
   * Esta función obtiene un usuario por medio de su id
   * @param id id del usuario a obtener 
   * @returns {Observable<Usuario>} retorna un observable con el usuario obtenido
   */
  getusuario(id: any): Observable<Usuario> {
    return this.http.get<Usuario>(`${this.urlEndPoint}/${id}`).pipe(
      catchError(e => {
        if (e.status != 401 && e.error.mensaje) {
          this.router.navigate(['/usuarios']);
          const textoerror = e.error.mensaje;
          console.error(textoerror)
        }
        return throwError(e);
      })
    )
  }
  /**
   * Esta función actualiza un usuario de la base de datos
   * @param  {Usuario} usuario usuario que se va actualizar
   * @returns {Observable<any>} retorna un observable de tipo any
   */
  update(usuario: Usuario): Observable<any> {
    return this.http.put<any>(`${this.urlEndPoint}/${usuario.id_usuario}`, usuario).pipe(
      catchError(e => {

        if (e.error.mensaje) {
          console.error(e.error.mensaje)
        }
        if (e.status == 400) {
          return throwError(e);
        }

        return throwError(e);
      })
    )
  }

}
