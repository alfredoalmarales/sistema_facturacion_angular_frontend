// Implementamos un interceptor http para manejar las respuestas validar los codigos de error 401 y 403 y evitar repetir codigos

import { Injectable } from '@angular/core'

import {
    HttpEvent, HttpInterceptor, HttpHandler, HttpRequest
} from '@angular/common/http'

import { Observable, throwError } from 'rxjs'
import { AuthService } from '../services/auth.service';

import Swal from 'sweetalert2'
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';

/**
 * Implementamos este interceptor  para manejar las respuestas y validar los codigos 401 y 403
 */
@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(
        private authService: AuthService,
        private router: Router,

    ) { }

    intercept(req: HttpRequest<any>, next: HttpHandler):
        Observable<HttpEvent<any>> {

        return next.handle(req).pipe(
            catchError(e => {
                if (e.status == 401) {
                    // si el token espira
                    if (this.authService.isAuthenticated()) {
                        this.authService.logout()
                    }
                    this.router.navigate([''])
                }
                // si esta autentificado pero no tiene permiso
                if (e.status == 403) {
                    const texto = "Hola " + this.authService.usuario.username + ", no tienes acceso a este recurso"
                    Swal.fire({
                        title: "Acceso denegado",
                        html: texto,
                        icon: "warning",
                        // confirmButtonColor: "#FF6D00"
                    });
                    this.router.navigate([''])
                }

                return throwError(e);
            })
        );
    }

}