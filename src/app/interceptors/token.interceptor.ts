import {Injectable} from '@angular/core'

import {
    HttpEvent,HttpInterceptor,HttpHandler,HttpRequest
} from '@angular/common/http'

import { Observable } from 'rxjs'
import { AuthService } from '../services/auth.service';

/**
 * Implementamos este interceptor para agregar el Authorization Bearer en las cabeceras Http 
 */
@Injectable()
export class TokenInterceptor implements HttpInterceptor{

    constructor(private authService: AuthService){}
    intercept(req: HttpRequest<any>, next: HttpHandler):
     Observable<HttpEvent<any>> {
       let token = this.authService.token;
       
       if(token != ""){
           const authReq = req.clone({
               headers:req.headers.set('Authorization','Bearer '+token)
           });
           // si estamos autentificado  por cada request esta pasando nuestro token
        //    console.log("TokenInterceptor => Bearer "+token)
           return next.handle(authReq);

       }
        return next.handle(req);
    }
    
}