export class Producto {
    id?:number;
    nombre?:string;
    estado?: string;
    valor_unitario?: number;
    foto?:string;
    fecha_registro?:string
}
