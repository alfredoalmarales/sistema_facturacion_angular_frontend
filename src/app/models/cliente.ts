import { Factura } from "./factura";
import { TipoIdentificacion } from "./tipo-identificacion";

export class Cliente {
    id?: number;
    razon_social?: string;
    fecha_registro?:string;
    facturas?: Array<Factura>=[];
    foto?:string;
    identificacion?:string;
    tipo_identificacion?:TipoIdentificacion;
    estado?:string
}
