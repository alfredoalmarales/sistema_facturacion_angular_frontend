import { Cliente } from "./cliente";
import { DetalleFactura } from "./detalle-factura";

export class Factura {
    consecutivo?:number;
    createAt?: string;
    facturadetalle: Array<DetalleFactura>=[];
    cliente?: Cliente;
    total?:number;

    calcularGranTotal(): number {
        this.total = 0;
        this.facturadetalle.forEach((item: DetalleFactura) => {
          this.total! += item.calcularImporte();
        });
        return this.total;
      }
}
