import { Perfil } from "./perfil";

export class Usuario {

   id_usuario?: number;
   nombre?: string;
   apellido?:string;
   username?:string;
   password?: string;
   foto?:string;   
   fecha_registro?:string;
   perfil?: Perfil;
   enabled?:boolean;

  }
  