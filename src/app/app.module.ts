import { LOCALE_ID, NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// formulario
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
//Provedores
import {ClienteService} from './services/cliente.service'
import {UsuarioService} from './services/usuario.service';
//HTTP
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

//toars
import { ToastrModule } from 'ngx-toastr';

//Componentes
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { UsuariosComponent } from './pages/lista-usuarios/usuarios.component';
import { ClientesComponent } from './pages/lista-clientes/clientes.component';
import { CrearclienteComponent } from './pages/crear-cliente/crearcliente.component';
import { DetalleComponent } from './components/detalle-cliente/detalle.component';

//ngx boopstrap
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

//Interceptor
import {TokenInterceptor} from '../app/interceptors/token.interceptor'
import {AuthInterceptor} from './interceptors/auth.interceptor';
import { DetalleFacturaComponent } from './pages/detalle-factura/detalle-factura.component';
import { FacturasComponent } from './pages/crear-facturas/facturas.component';
//Material
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatMenuModule} from '@angular/material/menu';
import { PaginatorComponent } from './components/paginator/paginator.component';
import { ListaProductosComponent } from './pages/lista-productos/lista-productos.component';
import { DetalleProductoComponent } from './components/detalle-producto/detalle-producto.component';
import { CrearProductosComponent } from './pages/crear-productos/crear-productos.component';
import { FacturacionComponent } from './pages/facturacion/facturacion.component';
import { DetalleUsuarioComponent } from './components/detalle-usuario/detalle-usuario.component';
import { CrearUsuarioComponent } from './pages/crear-usuario/crear-usuario.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    ClientesComponent,
    UsuariosComponent,
    CrearclienteComponent,
    DetalleComponent,
    DetalleFacturaComponent,
    FacturasComponent,
    PaginatorComponent,
    ListaProductosComponent,
    DetalleProductoComponent,
    CrearProductosComponent,
    FacturacionComponent,
    DetalleUsuarioComponent,
    CrearUsuarioComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-right',
      timeOut: 2000,
    }),
    NgbModule,
    MatAutocompleteModule,MatInputModule,MatFormFieldModule,MatMenuModule
  ],
  providers: [
    ClienteService,
    UsuarioService,
    {provide:LOCALE_ID,useValue:'es'},
    {provide:HTTP_INTERCEPTORS,useClass:TokenInterceptor, multi:true},
    {provide:HTTP_INTERCEPTORS,useClass:AuthInterceptor, multi:true} 
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
