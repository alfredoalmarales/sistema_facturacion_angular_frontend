import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Producto } from 'src/app/models/producto';
import { ProductoService } from 'src/app/services/producto.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-crear-productos',
  templateUrl: './crear-productos.component.html',
  styleUrls: ['./crear-productos.component.css']
})
export class CrearProductosComponent implements OnInit {

  form!: FormGroup
 submitted = false;
 subiendoDatos=false;
 bandeditar=false;

 public producto: Producto= new Producto()

  constructor(
    private productoService: ProductoService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
  ) { }

  ngOnInit(): void {
    this.formInit()
    this.cargarproducto(); 
  }

  formInit(){
    this.form = this.formBuilder.group({
      nombre: ['',Validators.required],
      valor_unitario: ['',Validators.required],
      estado: ['',Validators.required],
    });
  }
  get f() { return this.form.controls }

  public cargarproducto():void{
    this.activatedRoute.params.subscribe(
      params => {
        let id = params['id']
        if(id){
          this.productoService.getProducto(id).subscribe(
            (producto)=>{
              this.producto.id=producto.id;
              this.form.setValue({
                nombre:producto.nombre,
                valor_unitario:producto.valor_unitario,
                estado:producto.estado,
              })
              this.bandeditar=true;
            }
          )
        }
      }
    )
  }

  public create():void{
    this.submitted = true;
    if(!this.form.invalid){
      const currentDate = new Date();
      const fechactual = formatDate(currentDate, 'yyyy-MM-dd', 'en-US');
        this.subiendoDatos=true; 
        this.producto={
          nombre:this.form.value['nombre'],
          valor_unitario:+this.form.value['valor_unitario'],
          estado:this.form.value['estado'],
          fecha_registro:fechactual
        }
        this.productoService.create(this.producto).subscribe(         
            producto =>{
            this.router.navigate(['/productos'])
            Swal.fire({
              title: "producto Registrado",
              html:  "El producto: "+producto.nombre+' ha sido creado con éxito',
              icon: "success",
            });
           },
           err => {
            this.toastr.error("error:"+err.status, 'Error al crear el producto');
            this.ResetVariables();
           }
         ) 
    }else{
         this.toastr.info('Atención!', 'Rellene todos los campos obligatorios');
    } 
  }

  update():void{
    this.submitted = true;
    if(!this.form.invalid){
    this.producto={
      id:this.producto.id,//si no se manda tiene problema en el id creo porque no puede ser null
      nombre:this.form.value['nombre'],
      valor_unitario:this.form.value['valor_unitario'],
      estado:this.form.value['estado'],
    }
    this.productoService.update(this.producto).subscribe(
      json => {
        this.router.navigate(['/productos'])
        Swal.fire({
          title: "producto Actualizado",
          html: json.mensaje + ': '+json.producto.nombre,
          icon: "success",
        });      
      },
      err => {
        this.toastr.error("error:"+err.status, 'Error al editar el producto');
        this.ResetVariables();
      }
    )
    }else{
      this.toastr.info('Atención!', 'Rellene todos los campos obligatorios');
    }
  }

  ResetVariables(){
    this.form.reset();
    this.formInit();
    this.submitted=false;
    this.subiendoDatos=false;
  }

}
