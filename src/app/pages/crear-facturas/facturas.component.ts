import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { map,switchMap } from 'rxjs/operators';
import { DetalleFactura } from 'src/app/models/detalle-factura';
import { Factura } from 'src/app/models/factura';
import { Producto } from 'src/app/models/producto';
import { ClienteService } from 'src/app/services/cliente.service';
import { FacturasService } from 'src/app/services/factura.service';
import { ProductoService } from 'src/app/services/producto.service';

import Swal from 'sweetalert2'

@Component({
  selector: 'app-facturas',
  templateUrl: './facturas.component.html',
  styleUrls: ['./facturas.component.css']
})
export class FacturasComponent implements OnInit {
  titulo:string='Nueva Factura'
  factura:Factura=new Factura();
  findproductoid="";
  
  autocompleteControl = new FormControl();
  productos: string[] = ['Gafas', 'Tv', 'cargador'];
  productosFiltrados!: Observable<Producto[]>;

  constructor(private clienteService:ClienteService,
    private activatedRoute:ActivatedRoute,
    private facturaService:FacturasService,
    private productoService:ProductoService,
    private toastr: ToastrService,
    private router:Router) { }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe(params =>{
      let clienteId=+params.get('clienteId')!;
      this.clienteService.getCliente(clienteId).subscribe(cliente => {
         this.factura.cliente=cliente
      })
    });

    this.productosFiltrados = this.autocompleteControl.valueChanges.pipe(
      map(value => typeof value === 'string'?value:value.nombre), 
      switchMap(value => value? this._filter(value): []) 
    );
  }


  private _filter(value: string): Observable<Producto[]>{
    const filterValue = value.toLowerCase();

    return this.facturaService.filtrarProductos(filterValue);
  }

  mostrarNombre(producto?:Producto):string | any{
    return producto? producto.nombre:undefined
  }
 
  buscarProductoid(){
    let producto:Producto;
    this.productoService.getProducto(this.findproductoid).subscribe(
      response => {
        producto=response;
        if(this.existeItem(producto.id)){
          this.incrementaCantidad(producto.id);
        }else{
          let nuevoItem = new DetalleFactura();
          nuevoItem.producto=producto;
          this.factura.facturadetalle.push(nuevoItem);
        }
      },error => {
        this.toastr.error(error.error.mensaje,"Error");
      }
    );
  }

  seleccionarProducto(event:MatAutocompleteSelectedEvent):void{
    let producto = event.option.value as Producto;
    
    if(this.existeItem(producto.id)){ 
      this.incrementaCantidad(producto.id);
    }else{ 
      let nuevoItem = new DetalleFactura();
      nuevoItem.producto=producto;
      this.factura.facturadetalle.push(nuevoItem);
    }
    this.autocompleteControl.setValue('');
    event.option.focus();
    event.option.deselect();
  }

  actualizarCantidad(id:any,event:any):void{
    let cantidad:number = event.target.value as number;
    if(cantidad==0){
      return this.eliminarItemFactura(id);
    }
    this.factura.facturadetalle= this.factura.facturadetalle
    .map((item:DetalleFactura)=>{
      if(id===item.producto?.id){
         item.cantidad=cantidad;
      }
      return item;
    });
  }

  existeItem(id:any):boolean{
    let existe = false;
    this.factura.facturadetalle.forEach((item : DetalleFactura) =>{
      if(id === item.producto?.id){
        existe = true;
      }
    });
    return existe;
  }
  incrementaCantidad(id:any):void{
    this.factura.facturadetalle= this.factura.facturadetalle
    .map((item:DetalleFactura)=>{
      if(id===item.producto?.id){
         ++item.cantidad!;
      }
      return item;
    });
  }

  eliminarItemFactura(id:any){
     this.factura.facturadetalle=this.factura.facturadetalle.filter((item:DetalleFactura)=>id !== item.producto?.id);
  }
  create(){
    this.facturaService.createFactura(this.factura).subscribe(factura =>{
      Swal.fire({
        title: "Success",
        html: "Factura creada con éxito",
        icon: "success",
      });
      this.router.navigate(['/facturas',factura.consecutivo])
    })
  }
 
}
