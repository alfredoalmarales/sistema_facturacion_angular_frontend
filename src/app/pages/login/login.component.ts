import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service'
import { Router } from '@angular/router';

import Swal from 'sweetalert2'
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form!: FormGroup
  submitted = false;
  constructor(
    private formBuilder: FormBuilder,
    private _AuthService: AuthService,
    private _Router: Router,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    if (this._AuthService.isAuthenticated()) {
      this._Router.navigate(['/clientes']);
      const texto = "hola " + this._AuthService.usuario.username + ", ya estas autenticado"
      this.toastr.info('Atención!', texto);
    } else {
      this.formInit()
    }
  }

  /**
   * Inicializamos el formulario login
   */
  formInit() {
    this.form = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }
  get f() { return this.form.controls }

  /**
   * función que sirve para loguearse,si la respuesta es satisfactoria se guardan 
   * los datos del usuario y su token el storage, luego redirige a la página de listado de clientes
   * de lo contrario te muestra un sweet alert diciendo que el usuario o la contraseña son incorrectas
  */
  onSubmit() {
    this.submitted = true;
    if (this.form.invalid) {
      console.log("verifique los campos")
    } else {
      const username = this.form.value['username']
      const password = this.form.value['password']
      this._AuthService.login(username, password).subscribe(
        response => {
          this._AuthService.guardarUsuario(response.access_token);
          this._AuthService.guardarToken(response.access_token);
          let usuario = this._AuthService.usuario;
          this._Router.navigate(['/clientes']);
          let rol = this._AuthService.usuario.perfil?.toString();
          let texto = "Hola " + rol?.substring(5) + " " + usuario.nombre;
          this.toastr.info('Bienvenido!', texto);
        },
        err => {
          if (err.status == 400) {
            Swal.fire({
              title: "Error login",
              html: "Usuario o clave incorrectas",
              icon: "error",
            });
          }
        }
      )
    }
  }

}
