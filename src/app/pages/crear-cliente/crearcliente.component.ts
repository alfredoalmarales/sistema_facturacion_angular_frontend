import { Component, OnInit } from '@angular/core';
import { Cliente } from 'src/app/models/cliente';
import {ClienteService} from '../../services/cliente.service'
import {Router,ActivatedRoute} from '@angular/router'
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service'


//Toars y sweetAlert
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2'
import { formatDate } from '@angular/common';
import { TipoIdentificacion } from 'src/app/models/tipo-identificacion';

@Component({
  selector: 'app-crearcliente',
  templateUrl: './crearcliente.component.html',
  styleUrls: ['./crearcliente.component.css']
})
export class CrearclienteComponent implements OnInit {
form!: FormGroup
submitted = false;
subiendoDatos=false;
tipoIdentificaciones?: TipoIdentificacion[];
bandeditar=false;

 public cliente: Cliente= new Cliente()
  constructor(
    private clienteService: ClienteService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,

    ) { }

  ngOnInit(): void {
    this.formInit()
    this.cargarCliente();
    this.clienteService.getTipoIdentificaciones().subscribe(tipoidentificaciones=>{
        this.tipoIdentificaciones=tipoidentificaciones;
    })
  }
  formInit(){
    this.form = this.formBuilder.group({
      razon: ['',Validators.required],
      tipoIdentificacion: ['',Validators.required],
      identificacion: ['',Validators.required],
    });
  }
  get f() { return this.form.controls }
   
  public cargarCliente():void{
    this.activatedRoute.params.subscribe(
      params => {
        let id = params['id']
        if(id){
          this.clienteService.getCliente(id).subscribe(
            (cliente)=>{
              this.cliente.id=cliente.id;
              this.form.setValue({
                razon:cliente.razon_social,
                tipoIdentificacion:cliente.tipo_identificacion,
                identificacion:cliente.identificacion,
              })
              this.bandeditar=true;
            }
          )
        }
      }
    )
  }

  public create():void{
    this.submitted = true;
    if(!this.form.invalid){
      const currentDate = new Date();
      const fechactual = formatDate(currentDate, 'yyyy-MM-dd', 'en-US');
        this.subiendoDatos=true; 
        this.cliente={
          razon_social:this.form.value['razon'],
          tipo_identificacion:this.form.value['tipoIdentificacion'],
          identificacion:this.form.value['identificacion'],
          estado:"1",
          fecha_registro:fechactual
        }
        this.clienteService.create(this.cliente).subscribe(         
            cliente =>{
            this.router.navigate(['/clientes'])
            Swal.fire({
              title: "Cliente Registrado",
              html:  "El cliente: "+cliente.razon_social+' ha sido creado con éxito',
              icon: "success",
            });
           },
           err => {
            this.toastr.error("error:"+err.status, 'Error al crear el cliente');
            this.ResetVariables();
           }
         ) 
    }else{
         this.toastr.info('Atención!', 'Rellene todos los campos obligatorios');
    } 
  }

  update():void{
    this.submitted = true;
    if(!this.form.invalid){
    this.cliente={
      id:this.cliente.id,
      razon_social:this.form.value['razon'],
      tipo_identificacion:this.form.value['tipoIdentificacion'],
      identificacion:this.form.value['identificacion'],
      facturas:[], 
    }

    this.clienteService.update(this.cliente).subscribe(
      json => {
        this.router.navigate(['/clientes'])
        Swal.fire({
          title: "Cliente Actualizado",
          html: json.mensaje + ': '+json.cliente.razon_social,
          icon: "success",
        });      
      },
      err => {
        this.toastr.error("error:"+err.status, 'Error al editar el usuario');
        this.ResetVariables();

      }
    )
  }else{
    this.toastr.info('Atención!', 'Rellene todos los campos obligatorios');
  }
}




  ResetVariables(){
    this.form.reset();
    this.formInit();
    this.submitted=false;
    this.subiendoDatos=false;
  }
  compararTipoId(o1: TipoIdentificacion, o2: TipoIdentificacion): boolean {
    if (o1 === undefined && o2 === undefined) {
      return true;
    }

    return o1 === null || o2 === null || o1 === undefined || o2 === undefined ? false : o1.abreviatura === o2.abreviatura;
  }
}
