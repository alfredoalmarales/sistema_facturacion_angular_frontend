import { formatDate } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Perfil } from 'src/app/models/perfil';
import { Usuario } from 'src/app/models/usuario';
import { AuthService } from 'src/app/services/auth.service';
import { UsuarioService } from 'src/app/services/usuario.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-crear-usuario',
  templateUrl: './crear-usuario.component.html',
  styleUrls: ['./crear-usuario.component.css']
})
export class CrearUsuarioComponent implements OnInit {
  form!: FormGroup
  submitted = false;
  subiendoDatos = false;
  tiposPerfiles?: Perfil[];
  bandeditar=false;

  public usuario: Usuario = new Usuario()

  constructor(
    private usuarioService: UsuarioService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
  ) { }

  ngOnInit(): void {
    this.formInit();
    this.cargarUsuario();

    this.usuarioService.getTipoPerfiles().subscribe(tiposPerfiles=>{
      this.tiposPerfiles=tiposPerfiles;
    })
  }
  formInit() {
    this.form = this.formBuilder.group({
      nombre: ['', Validators.required],
      apellido: ['', Validators.required],
      nombreusuario: ['', Validators.required],
      contrasena: ['', Validators.required],
      perfil: ['', Validators.required],

    });
  }
  get f() { return this.form.controls }

  public cargarUsuario(): void {
    this.activatedRoute.params.subscribe(
      params => {
        let id = params['id']
        if (id) {
          this.usuarioService.getusuario(id).subscribe(
            (usuario) => {
              this.usuario.id_usuario = usuario.id_usuario;
              this.form.setValue({
                nombre: usuario.nombre,
                apellido: usuario.apellido,
                nombreusuario: usuario.username,
                contrasena: " ",
                perfil:usuario.perfil
              })
              this.bandeditar=true;
            }
          )
        }
      }
    )
  }

  public create(): void {
    this.submitted = true;
    if (!this.form.invalid) {
      const currentDate = new Date();
      const fechactual = formatDate(currentDate, 'yyyy-MM-dd', 'en-US');
      this.subiendoDatos = true;
      this.usuario = {
        nombre: this.form.value['nombre'],
        apellido: this.form.value['apellido'],
        username: this.form.value['nombreusuario'],
        password: this.form.value['contrasena'],
        perfil:this.form.value['perfil'],
        enabled:true,
        fecha_registro: fechactual
      }
      this.usuarioService.create(this.usuario).subscribe(
        usuario => {
          this.router.navigate(['/usuarios'])
          Swal.fire({
            title: "usuario Registrado",
            html: "El usuario: " + usuario.username + ' ha sido creado con éxito',
            icon: "success",
          });
        },
        err => {
          console.error('Codigo del error: ' + err.status);
          this.toastr.error(err.error.mensaje, "Error al crear el usuario")
          this.ResetVariables();
        }
      )
    } else {
      this.toastr.info('Atención!', 'Rellene todos los campos obligatorios');
    }

  }

  update():void{
    this.submitted = true;
    if(!this.form.invalid){
    this.usuario={
      id_usuario:this.usuario.id_usuario,
      nombre:this.form.value['nombre'],
      apellido:this.form.value['apellido'],
      username:this.form.value['nombreusuario'],
      password:this.form.value['contrasena'],
      perfil:this.form.value['perfil']
    }
    this.usuarioService.update(this.usuario).subscribe(
      json => {
        this.router.navigate(['/usuarios'])
        Swal.fire({
          title: "usuario Actualizado",
          html: json.mensaje + ': '+json.usuario.username,
          icon: "success",
        });      
      },
      err => {
        this.toastr.error("error:"+err.status, 'Error al editar el usuario');
        this.ResetVariables();
      }
    )
    }else{
      this.toastr.info('Atención!', 'Rellene todos los campos obligatorios');
    }
  }

  compararTipoId(o1: Perfil, o2: Perfil): boolean {
    if (o1 === undefined && o2 === undefined) {
      return true;
    }

    return o1 === null || o2 === null || o1 === undefined || o2 === undefined ? false : o1.nombre === o2.nombre;
  }

  ResetVariables(){
    this.form.reset();
    this.formInit();
    this.submitted=false;
    this.subiendoDatos=false;
  }

}
