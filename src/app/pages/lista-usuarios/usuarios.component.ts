import { Component, OnInit } from '@angular/core';
import { ModalService } from 'src/app/services/modal.service';
import { AuthService } from 'src/app/services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { UsuarioService } from 'src/app/services/usuario.service';
import { Usuario } from 'src/app/models/usuario';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit {
  entidad = "Usuarios"

  constructor(
    private usuarioservice: UsuarioService,
    public authService: AuthService,
    private modalService: ModalService,
    private toastr: ToastrService,
    private activatedRoute: ActivatedRoute,
    private route: Router,
  ) { }

  usuarios!: Usuario[];
  usarioSeleccionado!: Usuario;
  paginador: any;

  paramNombrebuscado = "";
  bandmostrarpaginacion = false;
  nombreUsarioBuscar = "";


  ngOnInit() {
    this.activatedRoute.paramMap.subscribe(params => {  

      this.paramNombrebuscado = params.get('term')!;
      let page: number = +params.get('page')!;


      if (!this.paramNombrebuscado) { 
        this.paramNombrebuscado = "";
      }

      if (!page) {
        page = 0;
      }
      this.usuarioservice.getUsuariosByName(this.paramNombrebuscado, page)
        .subscribe(response => {
          this.usuarios = response.content;
          this.paginador = response;
          this.bandmostrarpaginacion = true;
        });
    })

    this.modalService.notificarUpload.subscribe(usuario => {
      this.usuarios = this.usuarios.map(usuarioOriginal => {
        if (usuario.id == usuarioOriginal.id_usuario) {
          usuarioOriginal.foto = usuario.foto;
        }
        return usuarioOriginal;
      })
    })
  }

  abrirModal(usuario: Usuario) {
    this.usarioSeleccionado = usuario;
    this.modalService.abrirModal();
  }

  activarPaginacionBusqueda() {
    this.bandmostrarpaginacion = false;
    this.route.navigate(["usuarios/page", 0, this.nombreUsarioBuscar])
  }

}
