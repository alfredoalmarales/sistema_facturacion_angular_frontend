import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Factura } from 'src/app/models/factura';
import { FacturasService } from 'src/app/services/factura.service';

import { jsPDF } from "jspdf";
import html2canvas from 'html2canvas';

@Component({
  selector: 'app-detalle-factura',
  templateUrl: './detalle-factura.component.html',
  styleUrls: ['./detalle-factura.component.css']
})
export class DetalleFacturaComponent implements OnInit {
  factura?:Factura;
  titulo:string="Factura";
  constructor(private facturaService:FacturasService,
    private activatedRoute:ActivatedRoute) { }

  ngOnInit(): void {
    //obtenemos los parametros que se pasan por la ruta
    this.activatedRoute.paramMap.subscribe(params =>{
      let id =params.get('id');
      this.facturaService.getFactura(id).subscribe(factura=>{
          this.factura=factura;
      })
    })
  }

  downloadPDF() {
    const DATA:any= document.getElementById('htmlData');
    const doc = new jsPDF('p', 'pt', 'a4');
    const options = {
      background: 'white',
      scale: 3
    };
    html2canvas(DATA, options).then((canvas) => {

      const img = canvas.toDataURL('image/PNG');

      const bufferX = 15;
      const bufferY = 15;
      const imgProps = (doc as any).getImageProperties(img);
      const pdfWidth = doc.internal.pageSize.getWidth() - 2 * bufferX;
      const pdfHeight = (imgProps.height * pdfWidth) / imgProps.width;
      doc.addImage(img, 'PNG', bufferX, bufferY, pdfWidth, pdfHeight, undefined, 'FAST');
      return doc;
    }).then((docResult) => {
      docResult.save(`${new Date().toISOString()}_ReporteFactura.pdf`);
    });
  }

}
