import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TipoIdentificacion } from 'src/app/models/tipo-identificacion';
import { ClienteService } from 'src/app/services/cliente.service';

import { ToastrService } from 'ngx-toastr';
import { Cliente } from 'src/app/models/cliente';
import { ModalService } from 'src/app/services/modal.service';

@Component({
  selector: 'app-facturacion',
  templateUrl: './facturacion.component.html',
  styleUrls: ['./facturacion.component.css']
})
export class FacturacionComponent implements OnInit {
  form!: FormGroup
  submitted = false;
  subiendoDatos=false;
  tipoIdentificaciones?: TipoIdentificacion[];
  tipoid?:TipoIdentificacion;
  cliente?:Cliente;
  bandClienteEncontrado=false;

  constructor(private formBuilder: FormBuilder,
    private clienteService: ClienteService,
    private toastr: ToastrService,
    private modalService: ModalService,

    ) { }

  ngOnInit(): void {
    this.formInit();
    this.clienteService.getTipoIdentificaciones().subscribe(tipoidentificaciones=>{
      this.tipoIdentificaciones=tipoidentificaciones;
  })
  }
  formInit(){
    this.form = this.formBuilder.group({
      tipoIdentificacion: ['',Validators.required],
      identificacion: ['',Validators.required],
    });
  }

  get f() { return this.form.controls }
   
  Buscar(){
    this.submitted = true;
    if(!this.form.invalid){
      this.subiendoDatos=true; 
      this.tipoid=this.form.value['tipoIdentificacion'];
      let iden = this.form.value['identificacion'];
      this.clienteService.findCliente(this.tipoid?.id as number,iden).subscribe(
        cliente =>{
          this.bandClienteEncontrado=true;
          this.cliente=cliente;
          this.abrirModal()
          this.resetVariables();
        },error=>{
          this.toastr.error(error.error.mensaje,"Error");
          this.resetVariables();
        }
      );
    }
  }
  abrirModal() {
    this.modalService.abrirModal();
  }
  resetVariables(){
    this.submitted = false;  
    this.subiendoDatos=false; 
  } 
}
