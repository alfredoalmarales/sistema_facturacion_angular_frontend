import { Component, OnInit } from '@angular/core';
import { Cliente } from '../../models/cliente';
import { ClienteService } from '../../services/cliente.service'
import { ModalService } from 'src/app/services/modal.service';
import { AuthService } from 'src/app/services/auth.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {
  entidad = "Clientes"
  constructor(
    private clienteservice: ClienteService,
    public authService: AuthService,
    private modalService: ModalService,
    private toastr: ToastrService,
    private activatedRoute: ActivatedRoute,
    private route: Router,

  ) { }
  clientes!: Cliente[];
  clienteSeleccionado!: Cliente;
  paginador: any;

  paramNombrebuscado = "";
  bandmostrarpaginacion = false;
  nombreClienteBuscar = "";


  ngOnInit() {

    this.activatedRoute.paramMap.subscribe(params => {  

      this.paramNombrebuscado = params.get('term')!;
      let page: number = +params.get('page')!;


      if (!this.paramNombrebuscado) {
        this.paramNombrebuscado = "";
      }

      if (!page) {
        page = 0;
      }

      this.clienteservice.getClientesByName(this.paramNombrebuscado, page).subscribe(
        response => {
          this.clientes = response.content;
          this.paginador = response;
          this.bandmostrarpaginacion = true;
        });
       })

    this.modalService.notificarUpload.subscribe(cliente => {
      this.clientes = this.clientes.map(clienteOriginal => {
        if (cliente.id == clienteOriginal.id) {
          clienteOriginal.foto = cliente.foto;
        }
        return clienteOriginal;
      })
    })
  }


  abrirModal(cliente: Cliente) {
    this.clienteSeleccionado = cliente;
    this.modalService.abrirModal();
  }

  activarPaginacionBusqueda() {
    this.bandmostrarpaginacion = false;
    this.route.navigate(["clientes/page", 0, this.nombreClienteBuscar])
  }

}
