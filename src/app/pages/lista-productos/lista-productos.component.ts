import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';
import { Producto } from 'src/app/models/producto';
import { AuthService } from 'src/app/services/auth.service';
import { ProductoService } from 'src/app/services/producto.service';
import { ToastrService } from 'ngx-toastr';
import { ModalService } from 'src/app/services/modal.service';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-lista-productos',
  templateUrl: './lista-productos.component.html',
  styleUrls: ['./lista-productos.component.css']
})
export class ListaProductosComponent implements OnInit {

  entidad = "Productos"
  productos!: Producto[];
  paginador: any;
  productoSeleccionado!: Producto;
  formbusqueda!: FormGroup
  bandmostrarpaginacion = false;
  paramNombrebuscado = "";
  nombreProductoBuscar = "";

  constructor(
    private activatedRoute: ActivatedRoute,
    private productoService: ProductoService,
    public authService: AuthService,
    private modalService: ModalService,
    private route: Router,

  ) { }

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe(params => {
      this.paramNombrebuscado = params.get('term')!;
      let page: number = +params.get('page')!;
      if (!this.paramNombrebuscado) {
        this.paramNombrebuscado = "";
      }
      if (!page) {
        page = 0;
      }
      this.productoService.getProductosByName(this.paramNombrebuscado, page)
        .subscribe(response => {
          this.productos = response.content;
          this.paginador = response;
          this.bandmostrarpaginacion = true;
        });
    });
    this.modalService.notificarUpload.subscribe(producto => {
      this.productos = this.productos.map(productoOriginal => {
        if (producto.id == productoOriginal.id) {
          productoOriginal.foto = producto.foto;
        }
        return productoOriginal;
      })
    })
  }

  abrirModal(producto: Producto) {
    this.productoSeleccionado = producto;
    this.modalService.abrirModal();
  }

  activarPaginacionBusqueda() {
    this.bandmostrarpaginacion = false;
    this.route.navigate(["productos/page", 0, this.nombreProductoBuscar])
  }

}
