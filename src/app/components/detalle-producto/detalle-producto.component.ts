import { HttpEventType } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Producto } from 'src/app/models/producto';
import { AuthService } from 'src/app/services/auth.service';
import { ModalService } from 'src/app/services/modal.service';
import { ProductoService } from 'src/app/services/producto.service';
import { UploadfileService } from 'src/app/services/uploadfile.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-detalle-producto',
  templateUrl: './detalle-producto.component.html',
  styleUrls: ['./detalle-producto.component.css']
})
export class DetalleProductoComponent implements OnInit {
  titulo: string = "Detalle del Producto"
  @Input() producto?: Producto;
  public fotoSeleccionada?: File;
  labelinputfoto: string = "Selecionar foto";
  progreso: number = 0;


  constructor(private productoService: ProductoService,
    public modalService: ModalService,
    public authService: AuthService,
    private toastr: ToastrService,
    private uploadFile: UploadfileService
  ) { }

  ngOnInit(): void {
  }

  seleccionarFoto(event: any) {
    this.fotoSeleccionada = event.target.files[0];
    if (this.fotoSeleccionada) {
      this.labelinputfoto = this.fotoSeleccionada?.name;
    }
    this.progreso = 0;
    if (this.fotoSeleccionada?.type.indexOf('image')! < 0) {
      this.toastr.info("El archivo debe ser de tipo imagen", "Error")
      this.fotoSeleccionada != null;
    }

  }

  subirFoto() {

    if (!this.fotoSeleccionada) {
      Swal.fire({
        title: "Error",
        html: "Debe seleccionar una foto",
        icon: "error",
      });
    } else {

      this.uploadFile.subirFoto(this.fotoSeleccionada!, +this.producto?.id!, "productos")
        .subscribe((event: any) => {
          if (event.type === HttpEventType.UploadProgress) {
            this.progreso = Math.round((event.loaded / event.total) * 100);
          } else if (event.type === HttpEventType.Response) {
            let response: any = event.body
            this.producto = response.Producto as Producto
            this.modalService.notificarUpload.emit(this.producto);
            this.toastr.success('Foto subida!', response.mensaje);
            this.fotoSeleccionada = undefined;
            this.labelinputfoto = "Selecionar foto";
          }
        }
        )
    }
  }

  cerrarModal() {
    this.modalService.cerrarModal();
    this.progreso = 0;
    this.fotoSeleccionada = undefined;
    this.labelinputfoto = "Selecionar foto";
  }


}
