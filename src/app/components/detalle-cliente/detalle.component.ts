import { HttpEventType } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Cliente } from 'src/app/models/cliente';
import { Factura } from 'src/app/models/factura';
import { AuthService } from 'src/app/services/auth.service';
import { ClienteService } from 'src/app/services/cliente.service';
import { FacturasService } from 'src/app/services/factura.service';
import { ModalService } from 'src/app/services/modal.service';
import { UploadfileService } from 'src/app/services/uploadfile.service';

import Swal from 'sweetalert2'

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.css']
})
export class DetalleComponent implements OnInit {
  titulo: string = "Detalle del Cliente"
  @Input() cliente?: Cliente;
  public fotoSeleccionada?: File;
  labelinputfoto: string = "Selecionar foto";
  progreso: number = 0;
  constructor(
    private clienteService: ClienteService,
    public modalService: ModalService,
    private facturaService: FacturasService,
    public authService: AuthService,
    private toastr: ToastrService,
    private uploadFile: UploadfileService

  ) { }

  ngOnInit(): void {
  }

  seleccionarFoto(event: any) {
    this.fotoSeleccionada = event.target.files[0];
    if (this.fotoSeleccionada) {
      this.labelinputfoto = this.fotoSeleccionada?.name;
    }
    this.progreso = 0;
    if (this.fotoSeleccionada?.type.indexOf('image')! < 0) {
      Swal.fire({
        title: "Error",
        html: "El archivo dede ser de tipo imagen",
        icon: "error",
      });
      this.fotoSeleccionada != null;
    }

  }
  subirFoto() {

    if (!this.fotoSeleccionada) {
      Swal.fire({
        title: "Error",
        html: "Debe seleccionar una foto",
        icon: "error",
      });
    } else {

      this.uploadFile.subirFoto(this.fotoSeleccionada!, +this.cliente?.id!,"clientes")
        .subscribe((event: any) => {
          if (event.type === HttpEventType.UploadProgress) {
            this.progreso = Math.round((event.loaded / event.total) * 100);
          } else if (event.type === HttpEventType.Response) {
            let response: any = event.body
            this.cliente = response.Cliente as Cliente
            this.modalService.notificarUpload.emit(this.cliente);
            this.toastr.success('Foto subida!', response.mensaje);
            this.fotoSeleccionada = undefined;
            this.labelinputfoto = "Selecionar foto";
          }
        }
        )
    }
  }

  cerrarModal() {
    this.modalService.cerrarModal();
    this.progreso = 0;
    this.fotoSeleccionada = undefined;
    this.labelinputfoto = "Selecionar foto";
  }
  delete(factura: Factura): void {
    Swal.fire({
      title: '¿Estás seguro?',
      text: "¿Seguro que quieres elimiar la factura?!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Sí , Eliminar!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.facturaService.deleteFactura(factura.consecutivo!).subscribe(
          () => {
            this.cliente!.facturas = this.cliente?.facturas?.filter((fac: any) => fac !== factura)
            Swal.fire(
              'Eliminada!',
              'La factura se ha eliminado con éxito.',
              'success'
            )
          }
        )

      }
    })
  }
}