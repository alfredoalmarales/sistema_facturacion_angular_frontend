import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';

import Swal from 'sweetalert2'
import {Router,ActivatedRoute} from '@angular/router'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  title: string = 'App Angular'
  collapsed = true;
  constructor(
    private router: Router,
    public authService: AuthService
    ) { }

    // public authService: AuthService,
  ngOnInit(): void {
  }

  logout():void{
    this.authService.logout();
    this.router.navigate([''])
  }

}
