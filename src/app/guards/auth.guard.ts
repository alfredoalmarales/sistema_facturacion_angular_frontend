import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})

export class AuthGuard implements CanActivate {

  constructor(
    private authService: AuthService,
    private router: Router
     ){}

  /**
   * Para proteger nuestras rutas con este guard verificamos si el usuario esta autentificado,
   * Si no esta autentificado o esta autentificado pero ya  expiro su token   le niega el acceso y lo dirige a la pagina principal
   */
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean { 
      if(this.authService.isAuthenticated()){
        if(this.isTokenExpirado()){
          this.authService.logout();
          this.router.navigate(['']);
          return false
        }
        return true
      }
      this.router.navigate(['']);
      return false;
  }

  /**
   * Esta función nos sirve para detectar si el token del usuario a expirado
   * @returns {boolean} retorna true si expiro y false si no
   */
  isTokenExpirado(): boolean{
    let token = this.authService.token;
    let payload = this.authService.obtenerDatosToken(token);
    let fechanow = new Date().getTime()/1000;

    if(payload.exp < fechanow){
       return true;
    }
    return false;
  }
  
}
