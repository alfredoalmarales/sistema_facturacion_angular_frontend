import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';

import Swal from 'sweetalert2'
@Injectable({
  providedIn: 'root'
})
export class PerfilGuard implements CanActivate {
  constructor(
    private authService: AuthService,
    private router: Router) { }

  /**
   * Para verificar el rol de un usuario antes de acceder alguna ruta
   */
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    if (!this.authService.isAuthenticated()) {
      this.router.navigate([''])
      return false
    }

    let perfil = route.data['perfil'] as string;
    if (this.authService.tienePerfil(perfil)) {
      return true;
    }

    const texto = "Hola " + this.authService.usuario.username + ", no tienes acceso a este recurso"
    Swal.fire({
      title: "Acceso denegado",
      html: texto,
      icon: "warning",
    });
    this.router.navigate(['/clientes'])

    return false;
  }

}
