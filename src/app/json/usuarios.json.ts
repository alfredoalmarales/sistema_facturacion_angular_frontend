import { Usuario } from '../models/usuario';

export const USUARIOS: Usuario[] = [
  { id_usuario: 1, nombre: 'Andrés', apellido: 'Guzmán', usuario: 'profesor@bolsadeideas.com', contrasena: '2017-12-11' },
  { id_usuario: 2, nombre: 'Mr. John', apellido: 'Doe', usuario: 'john.doe@gmail.com', contrasena: '2017-11-11' },
  { id_usuario: 3, nombre: 'Linus', apellido: 'Torvalds', usuario: 'linus.torvalds@gmail.com', contrasena: '2017-11-12' },
  { id_usuario: 4, nombre: 'Rasmus', apellido: 'Lerdorf', usuario: 'rasmus.lerdorf@gmail.com', contrasena: '2017-11-13' },
  { id_usuario: 5, nombre: 'Erich', apellido: 'Gamma', usuario: 'erich.gamma@gmail.com', contrasena: '2017-11-14' },
  { id_usuario: 6, nombre: 'Richard', apellido: 'Helm', usuario: 'richard.helm@gmail.com', contrasena: '2017-11-15' },
  { id_usuario: 7, nombre: 'Ralph', apellido: 'Johnson', usuario: 'ralph.johnson@gmail.com', contrasena: '2017-11-16' },
  { id_usuario: 8, nombre: 'Bombasto', apellido: 'Vlissides', usuario: 'bombasto.vlissides@gmail.com', contrasena: '2017-11-17' },
  { id_usuario: 9, nombre: 'Dr James', apellido: 'Gosling', usuario: 'james.gosling@gmail.com', contrasena: '2017-11-18' },
  { id_usuario: 10, nombre: 'Magma', apellido: 'Lee', usuario: 'magma.lee@gmail.com', contrasena: '2017-11-19' },
  { id_usuario: 11, nombre: 'Tornado', apellido: 'Roe', usuario: 'tornado.roe@gmail.com', contrasena: '2017-11-20' }
];
